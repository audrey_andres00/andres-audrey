console.log("Array Manipulation!")

//Array Methods
	//JS has built in functions and methods for arrays. This allows us to manipulate and access array types.

	//Mutator Methods
	/*
		- are functions that mutate or change array after they are created
		- these methods manipulate the original array performing various tasks such as adding and removing elements
	*/

	//array
	let fruits = ['Apple','Orange','Kiwi','Dragon Fruit'];
	console.log(fruits);

//push()
//Adds an element in the END of an array AND returns the array's LENGTH
//Syntax:
	// arrayName.push("itemToPush");

	console.log('Current array:');
	console.log(fruits);

	//if we assign it to a variable, we are able to save the length of the array
	let fruitsLength = fruits.push('Mango');
	console.log(fruitsLength);
	console.log('Mutated array from push method:');
	console.log(fruits);

	//we can push multiple items at the same time

//pop()
//Removes the last element in an array and returns the removed element

//Syntax:
	//arrayName.pop();

	let removedFruit = fruits.pop();
	console.log(removedFruit);
	console.log('Mutated array from pop method:');
	console.log(fruits);

	//Mini Activity 1

	function removeFruit(){
		fruits.pop();
	}

	removeFruit();
	console.log(fruits);//deleted Dragon Fruit

//unshift()
//adds one or more elements at the beginning of an array
//Syntax:
	//arrayName.unshift('elementA');
	//arrayName.unshift('elementA','elementB');

	fruits.unshift('Lime','Banana');//added Lime and Banana in front
	console.log('Mutated array from unshift method:');
	console.log(fruits);

	function unshiftFruit(fruit1, fruit2){
		fruits.unshift(fruit1, fruit2);
	}

	unshiftFruit("Grapes","Papaya");
	console.log(fruits);

//shift()
//removes an element at the beginning of an array AND returns the removed element
//Syntax:
	//arrayName.shift();

	let anotherFruit = fruits.shift();
	console.log(anotherFruit);
	console.log('Mutated array from shift method:');
	console.log(fruits);

//splice()
//simultaneously removes elements from  a specified index and adds elements
//Syntax:
	//arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);

	fruits.splice(1, 2, 'Calamansi','Cherry');
	console.log('Mutated array from splice method:');
	console.log(fruits);

	function spliceFruit(index, deleteCount, fruit){
		fruits.splice(index, deleteCount, fruit);
	}

	spliceFruit(1, 1, 'Avocado');
	spliceFruit(2, 1, 'Durian');
	console.log(fruits);

//sort()
//re-arranges the array elements in alphanumeric order
//Syntax:
	//arrayName.sort();

	fruits.sort();
	console.log('Mutated array from sort method:');
	console.log(fruits);

//reverse()
//reverses the order of array elements (Z-A)
//Syntax:
	//arrayName.reverse();

	fruits.reverse();
	console.log('Mutated array from reverse method:');
	console.log(fruits);