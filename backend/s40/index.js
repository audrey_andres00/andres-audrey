/*
	npm init - used to initialize a new package.json file
	package.json - contains info about the project, dependencies, and other settings
	package-lock.json - locks the versions of all installed dependencies to its specific version
*/

const express = require("express")
const bodyParser = require("body-parser")
const app = express();
const port = 4000

//Middlewares
	//a software that provides common services and capabilities to application outside of what's offered by operating system

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

//allows our app to read json data
app.use(express.json())
//allows our application to read data from forms
//this also allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended:true}))

app.get("/",(req,res)=>{
	res.send("Hello World")
})

app.get("/hello",(req,res)=>{
	res.send("Hello from /hello endpoint")
})

app.post("/hello",(req,res)=>{
	res.send("Hello from the post route")
})

//Mini Activity 1
//Refactor the post route to receive data from the req.body
//Send a message that has the data received

	//`Hello firstName lastName`

app.post("/request",(req,res)=>{
	const {firstName,lastName} = req.body;
	res.send(`Hello ${firstName} ${lastName}`)
})

//Mini Activity 2

app.put("/",(req,res)=>{
	res.send("Hello from the ExpressJS PUT method route!")
})

app.delete("/",(req,res)=>{
	res.send("Hello from the ExpressJS DELETE method route!")
})
//array will store objects when the '/signup' route is accessed
let users = []

/*app.post("/signup",(req,res)=>{

	console.log(req.body)

	users.push(req.body)
	res.send(users)
})*/

//Mini Activity 3
app.post("/signup",(req,res)=>{

	if(req.body.username !== '' && req.body.password !== ''){
		users.push(req.body)
		res.send(`User ${req.body.username} has been successfully registered`)
	} else {
		res.send('Please input BOTH username and password.')
	}

})

app.put("/change-password",(req,res)=>{

	let message;

	for(let i=0; i<users.length; i++){

		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated!`
			console.log(users);
			break;
		} else {
			message = "User does not exist"
		}
	}

	res.send(message);
})


if(require.main === module){
	app.listen(port,()=>console.log(`Server running at port ${port}`))
}

module.exports = app;