console.log("Hello, B297!");

//Functions
	//Parameters and Arguments

	/*function printName(){

		let nickName = prompt("Enter your nickname: ");
		console.log("Hi, " + nickName);
	}

	printName();*/

	function printName(name){

		console.log("Hi, " + name);
	}

	printName("Audrey");

	let sampleVariable = "Cardo";

	printName(sampleVariable);


	function checkDivisibilityBy8(num){

		let remainder = num % 8;
		console.log("The remainder of " + num + " divided by 8 is: " + remainder);
		let isDivisibleBy8 = remainder === 0;
		console.log("Is " + num + " divisible by 8?");
		console.log(isDivisibleBy8);
	}

	checkDivisibilityBy8(64);
	checkDivisibilityBy8(28);
	checkDivisibilityBy8(9678);


	/*

		Mini-Activity
		check the divisibility of a number by 4
		parameter: num

		1. 56
		2. 95
		3. 444

	*/


	function checkDivisibilityBy4(num){

		let remainder = num % 4;
		console.log("The remainder of " + num + " divided by 4 is: " + remainder);
		let isDivisibleBy4 = remainder === 0;
		console.log("Is " + num + " divisible by 4?");
		console.log(isDivisibleBy4);
	}

	checkDivisibilityBy4(56);
	checkDivisibilityBy4(95);
	checkDivisibilityBy4(444);


	//Functions as arguments
	//Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(){
		argumentFunction();
	}

	//Adding and removing the parentheses "()" impacts the output of JS heavily
	//when a function is used with parentheses "()", it denotes invoking/calling a function
	//A function used without a parentheses is normally associated with using the function as an argument to another function

	invokeFunction(argumentFunction);

	//Using multiple parameters

	function createFullName(firstName, middleName, lastName){

		console.log(firstName + " " + middleName + " " + lastName);
	};

	createFullName('Juan','Dela','Cruz');
	createFullName('Juan','Dela','Cruz','III');

	//Using variables as arguments

	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName,middleName,lastName);

	/*
		create a function called printFriends


		parameters:
		friend1, friend2, friend3
		
	*/

	function printFriends(friend1, friend2, friend3){

		console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3);
	};

	printFriends('Linny','Tuck','Ming Ming');


	//Return Statement

	function returnFullName(firstName,middleName,lastName){

		return firstName + ' ' + middleName + ' ' + lastName;
		console.log("This will not be printed!");
	}

	let completeName1 = returnFullName("Monkey","D","Luffy");
	let completeName2 = returnFullName("Cardo","Tanggol","Dalisay");

	console.log(completeName1 + " is my bestfriend!");
	console.log(completeName2 + " is my friend!");


