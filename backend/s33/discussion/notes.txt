What is a Data Model?

A data model describes how data is organized and gruoped in our database

Data Modelling
	Data should have a purpose and its organization must be related to the kind of application we are building

Scenario:

	A course booking system application where a user can book to a course

	name
	student id
	payment method
	email
	address
	grade
	section
	course
	schedule
	advisor
	teacher
	room

	Features:

		- User Registration
		- User Authentication/Login

		Authenticated Users:
		View Active Course
		Enroll Course
		Update Details
		Delete Details

		Admin Users:
		Add Course
		Update Course
		Archive/De-activage Course
		Re-activate Course
		View all courses
		View and manage user accounts

		All Users (Guest, Authenticated, Admin):
		View Active Courses

	Data Models
		- Blueprints for our documents that we can follow and structure our data
		- Shows the relationship between our data

	user {
		id - unique identifier for the document,
		username,
		firstName,
		lastName,
		email,
		password,
		mobileNum,
		isAdmin,
		enrollments
	}

	course {
		id - unique identifier for the document,
		name,
		description,
		slots,
		price,
		isActive,
		enrollees
	}

	transactions {
		id - unique identifier for the document,
		userId (will come from the user),
		courseId (will come from the course),
		isPaid,
		paymentMethod,
		date
	}

	Model Relationships
	To be able to properly organize an application, we should also be able to identify the relationship between entities or between our models

	One to One - this relationship means that a model is exclusively related to only one model

	Employee:
	{
		"id":"2023Dev",
		"firstName":"Peter",
		"lastName":"Parker",
		"email":"imakewebs@gmail.com"
	}
	Credentials:
	{
		"id":"creds_01",
		"employee_id":"2023Dev",
		"role":"developer",
		"team":"dev team"
	}

	In MongoDB, one to one relationships can be expressed in other way instead of referencing.

	//Embedding - embed/put another document in a document
		//Subdocuments - are documents embedded in a parent document

		Employee:
		{
			"id":"2023Dev",
			"firstName":"Peter",
			"lastName":"Parker",
			"email":"imakewebs@gmail.com",
			"credentials": {
				"id":"creds_01",
				"role":"developer",
				"team":"dev team"
			}
		}

	One to Many

		One model is related to multiple other models
		However, the other models are only related to one

		Blog Posts - Comments

			A blog post can have multiple comments but each comment should only refer to a single blog post

		Blog: {
			"id":"blog7-25",
			"title":"Happy Tuesday!",
			"content":"We are happy",
			"createdOn":07/25/2023,
			"author":"blogwriter1"
		}

		Comments:
		{
			"id":"blogcomment1",
			"comment":"Awesome blog!!",
			"author":"blogwriter1",
			"blog_id":"blog-7-25"
		}
		{
			"id":"blogcomment2",
			"comment":"BOOOOOO. Your blog is not awesome!!!",
			"author":"notBasher1",
			"blog_id":"blog-7-25"
		}

		//Mini Activity

		Blog: {
			"id":"blog7-25",
			"title":"Happy Tuesday!",
			"content":"We are happy",
			"createdOn":07/25/2023,
			"author":"blogwriter1",
			"comments": [
				{
					"id":"blogcomment1",
					"comment":"Awesome blog!!",
					"author":"blogwriter1"
				},
				{
					"id":"blogcomment2",
					"comment":"BOOOOOO. Your blog is not awesome!!!",
					"author":"notBasher1"
				}
			]
		}

	Many to Many
	//Multiple documents are related to multiple documents
	users - courses
	When a "many to many relationship" is created, for models to relate to each other, an associative entity is created.

	With Referencing:

	user {
		id - unique identifier for the document,
		username,
		firstName,
		lastName,
		email,
		password,
		mobileNum,
		isAdmin,
		enrollments
	}

	course {
		id - unique identifier for the document,
		name,
		description,
		slots,
		price,
		isActive,
		enrollees
	}

	transactions {
		id - unique identifier for the document,
		userId (will come from the user),
		courseId (will come from the course),
		isPaid,
		paymentMethod,
		date
	}

	In MongoDB, many to many relationships can also be expressed in another way
	Two-way embedding - in two-way embedding, the associative entity is created and embedded in both models/documents

	user {
		id - unique identifier for the document,
		username,
		firstName,
		lastName,
		email,
		password,
		mobileNum,
		isAdmin,
		enrollments: [
			{
				id - unique identifier for the document,
				courseId (will come from the course),
				isPaid,
				paymentMethod,
				date
			}
		]
	}

	course {
		id - unique identifier for the document,
		name,
		description,
		slots,
		price,
		isActive,
		enrollees: [
			{
				id - unique identifier for the document,
				userId (will come from the user),
				isPaid,
				paymentMethod,
				date
			}
		]
	}