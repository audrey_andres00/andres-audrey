console.log("Array Traversal!");

//Arrays are used to store multiple realted data/values in a single variable
//for us to declare and create arrays, we use [] also known as "array literals"

let hobbies = ["Play League","Read a book","Listen to music","Code"];
console.log(typeof hobbies);
//Arrays are actually a special type of object
//Does it have key value pairs? Yes. index number : element
//Arrays make it easy to manage, manipulate as set of data
//Methods is another term for functions associated with an object and is used to execute statements that are relevant to a specific object
/*
	Syntax:

	let/const arrayName = [elementA, elementB, elementC ...]
*/

//Common examples of arrays
let grades = [98.5,94.6,90.8,88.9];
let computerBrands = ['Acer','Asus','Lenovo','HP','Neo','Redfox','Gateway','Toshiba','Fujitsu'];

let mixedArr = [12, 'Asus', null, undefined, {}];
let arraySample = ['Cardo','One Punch Man', 25000, false];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);
console.log(arraySample);

//Mini Activity

//1.
	let tasks = ["Watch anime","Play computer games","Play with my dogs","Read manga/manhwa","Sleep"];
	let capitalCities = ["Berlin","Athens","Tokyo","Cairo"];

	console.log(tasks);
	console.log(capitalCities);

//Alternative way to write arrays
let myTasks = [

		'drink html',
		'eat javascript',
		'inhale css',
		'bake react'
	];

//Create an array with values from variables:
let username1 = "gdragon22";
let username2 = "gdino21";
let username3 = "ladiesman217";
let username4 = "transformers45";
let username5 = "noobmaster68";
let username6 = "gbutiki78";

let guildMembers = [username1,username2,username3,username4,username5,username6];
console.log(guildMembers);
console.log(myTasks);

//[.length property]

//allows us to get and set the total number of items or elements in an array
//the .length property of an array gives the number data type

console.log(myTasks.length);//4
console.log(capitalCities.length);//4

let blankArray = [];
console.log(blankArray.length);

//length property can also be used with strings

let fullName = "Cardo Dalisay";
console.log(fullName.length);//13

//length property can also set the total number of items in an array

myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

//to delete a specific item in an array, we can employ array methods

//another example using decrementation
capitalCities.length--;
console.log(capitalCities);

//can we do the same with a string? no
fullName.length = fullName.length-1;
console.log(fullName.length);

let theBeatles = ["John","Paul","Ringo","George"];
theBeatles.length++
console.log(theBeatles);//last element is empty

theBeatles[4] = fullName;
console.log(theBeatles);

theBeatles[theBeatles.length] = "Tanggol";
console.log(theBeatles);

//[Read from Arrays]
/*
	Access elements with the use of their index
	Syntax:
	arrayName[index]
*/

console.log(capitalCities[0]);//accessed the first element of the array

console.log(grades[100]);

let lakerLegends = ["Kobe","Shaq","LeBron","Magic","Kareem"];
console.log(lakerLegends[2]);//3rd element
console.log(lakerLegends[4]);//5th element
//you can also save array items in another variable
let currentLaker = lakerLegends[2];
console.log(currentLaker);

console.log("Array before assignment");
console.log(lakerLegends);
lakerLegends[2] = "Pau Gasol";
console.log('Array after re-assignment');
console.log(lakerLegends);

//Mini Activity 2

let favoriteFoods = [
		"Tonkatsu",
		"Adobo",
		"Pizza",
		"Lasagna",
		"Sinigang"
	];

favoriteFoods[3] = "Sushi";
favoriteFoods[4] = "Cake";

console.log(favoriteFoods[3]);
console.log(favoriteFoods[4]);

//Mini Activity 3

function findBlackMamba(index){
	return lakerLegends[index];
}

let blackMamba = findBlackMamba(0);
console.log(blackMamba);//Kobe

//Mini Activity 4

function addTrainers(name){
	theTrainers[theTrainers.length] = name;
}

let theTrainers = ['Ash'];

addTrainers("Brock");
addTrainers("Misty");

console.log(theTrainers);

//Access the last element of an array
//since the first element of an array starts at 0, subtracting 1 to the length of the array will offset the value by one allowing us to get the last element

let bullsLegends = ["Jordan","Pippen","Rodman","Kukoc"];
let lastElementIndex = bullsLegends.length-1;
console.log(bullsLegends[lastElementIndex]);

//Add items into the array

let newArr = [];
console.log(newArr[0]);//undefined

newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

newArr[newArr.length-1] = "Aerith Gainsborough";
console.log(newArr);

/*
Mutator methods:

newArr.pop()
newArr.push()
newArr.shift()
newArr.unshift()
newArr.splice()
*/

//[Loop over an array]

for(let index = 0; index<newArr.length; index++){
	console.log(newArr[index])
}

let numArr = [5,12,30,48,40];

for(let index = 0; index<numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5!");
	} else{
		console.log(numArr[index] + " is not divisible by 5!");
	}
}

//[Multidimentional]

let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

//access elements of a multidimentional array
console.log(chessBoard[1][4]);//e2
console.log("Pawn moves to: " + chessBoard[1][5]);

console.log(chessBoard[7][0]);//a8
console.log(chessBoard[5][7]);//h6
console.log(chessBoard[0][0]);//a1