console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
    friends: {
        hoenn: ["May","Max"],
        kanto: ["Brock","Misty"]
    },
    talk: function(){
        // console.log("Pickachu! I choose you!");
        return "Pickachu! I choose you!";
    }
}

// Initialize/add the given object properties and methods

console.log(trainer);

// Properties

console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer['pokemon']);

// Methods

console.log("Result of talk method");
console.log(trainer.talk());

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name,level){
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;
    this.tackle = function(target){
            console.log(this.name + " tackled " + target.name);
            target.health = target.health - this.attack;

            if(target.health <= 0){
                return this.faint();
            }
            else {
                return target.name + "'s health is reduced to " + target.health;
            }
        }

    this.faint = function(){
            return this.name + " fainted";
    }
}

let pikachu = new Pokemon("Pikachu",12);
let geodude = new Pokemon("Geodude",8);
let mewtwo = new Pokemon("Mewtwo",100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

console.log(geodude.tackle(pikachu));
console.log(pikachu);

console.log(mewtwo.tackle(geodude));
console.log(geodude);
// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}