console.log("Intro to JSON!");

/*
	JSON (JS Object Notation)
		- also used in other programming languages

	Syntax:

	{
		"propertyA":"valueA",
		"proertyB":"valueB"
	};

	JSON are wrapped in curly braces
	Properties/keys are wrapped in double quotations
*/

	let sample1 = `

		{
			"name":"Jayson Derulo",
			"age":20,
			"address":{
				"city":"Quezon City",
				"country":"Philippines"
			}
		}
	`;

	console.log(sample1);
	console.log(typeof sample1);//string

	//Are we able to turn a JSON into a JS Object?
	//YES.

	console.log(JSON.parse(sample1));//object

	//JSON Array
		//JSON Array is an array of JSON

	let sampleArr = `

		[
			{
				"email":"wackywizard@example.com",
				"password":"laughOutLoud123",
				"isAdmin": true
			},
			{
				"email":"dalisay.cardo@gmail.com",
				"password":"alyanna4ever",
				"isAdmin": true
			},
			{
				"email":"lebroan4goat@ex.com",
				"password":"ginisamix",
				"isAdmin": false
			},
			{
				"email":"ilovejson@gmail.com",
				"password":"jsondontloveme",
				"isAdmin": false
			}
		]
	`

	//Can we use Array Methods on a JSON Array?
	//No. Because a JSON is a string
	console.log(typeof sampleArr);

	//So what can we do to be able to add more items/objects into our sampleArr?
	//PARSE the JSON array to a JS array and save it in a variable

	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);
	console.log(typeof parsedSampleArr);

	//Mini Activtity

	console.log(parsedSampleArr.pop());
	console.log(parsedSampleArr);

	//if for example, we need to send this data back to our client/frontend, it should be in JSON format

	//JSON.parse does not mutate/update the original JSON
		//we can actually turn a JS object into a JSON again

	//JSON.stringify() - this will stringify JS objects as JSON

	sampleArr = JSON.stringify(parsedSampleArr);
	console.log(sampleArr);
	console.log(typeof sampleArr);//string

	//Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON => frontend/client

	let jsonArr = `

		[
			"pizza",
			"hamburger",
			"spaghetti",
			"shanghai",
			"hotdog stick on a pineapple",
			"pancit bihon"
		]
	`

	//Mini Activity

	console.log(jsonArr);
	let parsedJsonArr = JSON.parse(jsonArr);
	parsedJsonArr.pop();
	parsedJsonArr.push("buko salad");
	jsonArr = JSON.stringify(parsedJsonArr);
	console.log(jsonArr);

	//Gather User Details

	let firstName = prompt("What is your first name?");
	let lasttName = prompt("What is your last name?");
	let age = prompt("What is your age?");
	let address = {
		city: prompt("Which city do you live in?"),
		country: prompt("Which country does your city address belong to?")
	};

	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lasttName,
		age: age,
		address: address
	})

	console.log(otherData);
	//console.log(typeof otherData);