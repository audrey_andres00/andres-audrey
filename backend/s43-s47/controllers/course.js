//[Activity - s45]

//Dependencies and Modules
const Course = require("../models/Course");
const User = require("../models/User")

//Create a new course
module.exports.addCourse = (req,res) => {

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	return newCourse.save().then((course,err)=>{

		if(err){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch(err => res.send(err))
}

//[End of Activity]

//Retrieve all courses
/*
	1. Retrieve all the courses from the database
*/

//We will use the find() method for our course model

module.exports.getAllCourses = (req,res) => {
  return Course.find({}).then(result =>{
    return res.send(result)
  })
}

//[Mini Activity]
//getAllActiveCourses
//create a function that will hannndle req,res
//that will find all active courses in the db
module.exports.getAllActiveCourses = (req,res) => {
	return Course.find({isActive: true}).then(result => {
		return res.send(result)
	})
}

//Get a specific course
module.exports.getCourse = (req,res) => {
	return Course.findById(req.params.courseId).then(result => {
		return res.send(result)
	})
}

//Edit a specific course
module.exports.updateCourse = (req,res) => {

	let updatedCourse = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	};

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course,error)=>{
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
}

//[Activity - S46]

//Course archiving
module.exports.archiveCourse = (req, res) =>{

	return Course.findByIdAndUpdate(req.params.courseId, {isActive: false}).then(result=>{
		return res.send(true)
	})
}

//Activating a course
module.exports.activateCourse = (req, res) =>{

	return Course.findByIdAndUpdate(req.params.courseId, {isActive: true}).then(result=>{
		return res.send(true)
	})
}

//[End of Activity]

// Controller action to search for courses by course name
module.exports.searchCoursesByName = async (req, res) => {
  try {
    const { courseName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const courses = await Course.find({
      name: { $regex: courseName, $options: 'i' }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

//Shorthand Syntax**
/*module.exports.searchCoursesByName = async (req, res) => {
	let courseName = req.body.courseName;

	return Course.find({name: {$regex: courseName, $options: 'i'}}).then(result => {res.send(result)}).catch(err => {console.log(err)})
}*/

//Get all enrolled user to a certain course
module.exports.getEmailsOfEnrolledUsers = async (req, res) => {
  const courseId = req.params.courseId; // Changed req.body.courseId to req.params.courseId

  try {
    // Find the course by courseId
    const course = await Course.findById(courseId);

    if (!course) {
      return res.status(404).json({ message: 'Course not found' });
    }

    // Get the userIds of enrolled users from the course
    const userIds = course.enrollees.map(enrollee => enrollee.userId);

    // Find the users with matching userIds
    const enrolledUsers = await User.find({ _id: { $in: userIds } }); // Changed users to userIds

    // Extract the emails from the enrolled users
    const emails = enrolledUsers.map(user => user.email); // Changed forEach to map

    res.status(200).json({ userEmails: emails }); // Changed userEmails to emails
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while retrieving enrolled users' });
  }
};

//[Activity S48]

//Retrieve all the courses within the given price range
// Controller action to search for courses by price range
module.exports.searchCoursesByPrice = async (req, res) => {
  try {
    const { minPrice, maxPrice } = req.body;

    // Search for courses with prices within the given range
    const courses = await Course.find({
      price: { $gte: minPrice, $lte: maxPrice }
    });

    res.json(courses);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
}

//[End of Activity]