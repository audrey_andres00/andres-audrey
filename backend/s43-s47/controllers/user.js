//Controller
//Dependencies and Modules
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

//Check if the email exists already
/*
	Steps:
	1. "find" mongoose method duplicate emails
	2. "then" method to send a response back to the frontend application based on the "find" method
*/

module.exports.checkEmailExists = (reqbody) =>{
	return User.find({email:reqbody.email}).then(result=>{

		//find method returns a record if a match is found
		if(result.length > 0){
			return true;
		}
		//no duplicate founnd
		//the user is not yet registered in the db
		else {
			return false;
		}
	})
}

//User Registration
/*
	Steps:
	1. create a new user object
	2. make sure that the password is encrypted
	3. save the new user to the database
*/

module.exports.registerUser = (reqbody) =>{

	let newUser = new User({

		firstName: reqbody.firstName,
		lastName: reqbody.lastName,
		email: reqbody.email,
		mobileNo: reqbody.mobileNo,
		password: bcrypt.hashSync(reqbody.password,10)
	})

	return newUser.save().then((user,error)=>{

		if(error){
			return false;
		} else {
			return true;
		}
	})
	.catch(err=>err)
}

//User authentication
/*
	Steps:
	1. Check the db if user email exists
	2. compare the password from req.body and password stored in the db
	3. generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req,res)=>{
	return User.findOne({email:req.body.email}).then(result=>{
		if(result===null){
			return false;
		} else {
			//compareSync() method is used to compare a non-ecrypted password from the encrypted password from the db
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}
			//else pw does not match
			else {
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}

//[Activity] User Document/Data
/*module.exports.getProfile = (req, res) =>{

	return User.findOne({_id: req.body.id}).then(result=>{
		result.password = ""
		return res.send(result)
	})
	.catch(err => res.send(err))
}*/
//[End of Activity]

//Retrieve user details
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Reassign the password of the returned document to an empty string
	3. Return the result back to the frontend
*/

module.exports.getProfile = (req, res) => {

	return User.findById(req.user.id)
	.then(result => {
		result.password = "";
		return res.send(result);
	})
	.catch(err => res.send(err))
};

//Enroll a user to a class
/*
	Steps:
	1. Find the document in the database using the user's ID
	2. Add the course ID to the user's enrollment array
	3. Update the document in the document in the MongoDB Atlas db
*/
module.exports.enroll = async (req, res) => {

	//to check in the terminal the user id and the courseId
	console.log(req.user.id);
	console.log(req.body.courseId);

	//checks if the user is an admin and deny the enrollment
	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	//creates an "isUserUpdated" variable and returns true upon successful update otherwise returns error
	let isUserUpdated = await User.findById(req.user.id).then(user => {
		
		//add the courseId in an object and push that object into the user's "enrollments" array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		//add the course in the "enrollments" array
		user.enrollments.push(newEnrollment);

		//save the updated user and return if successful or the error message if failed
		return user.save().then(user => true).catch(err => err.message);
	});

	//checks if there are error in updating the user
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	if(isCourseUpdated !== true){
		return res.send({ message: isCourseUpdated });
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: "Enrolled Successfully"})
	}
}

//[Activity - S47]

//retrieve all the courses in which the authenticated user is enrolled
module.exports.getEnrollments = (req, res) => {
	return User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err));
}

//[End of Activity]

//Resetting the password
module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json({ message: 'Password reset successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

//Mini-Activity - Contextualize the updateProfile controller
//Updating the user profile
module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { firstName, lastName, mobileNo } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { firstName, lastName, mobileNo },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
}

//[Activity S48]

//Update a regular user to an admin and send a message for successful update
// Controller function to update a user as an admin
module.exports.updateUserToAdmin = async (req, res) => {
  try {
    // Check if the authenticated user is an admin
    if (!req.user.isAdmin) {
      return res.status(403).json({ message: 'Access denied. Admin only.' });
    }

    const userIdToUpdate = req.body.userId;

    // Find the user by ID and update the isAdmin property to true
    const updatedUser = await User.findByIdAndUpdate(
      userIdToUpdate,
      { isAdmin: true },
      { new: true } // Returns the updated user
    );

    if (!updatedUser) {
      return res.status(404).json({ message: 'User not found.' });
    }

    res.status(200).json({ message: 'User updated as admin.' });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred.', error });
  }
};

//Update the enrollment status of a user for a specific course
// Controller function to update enrollment status
module.exports.updateEnrollmentStatus = async (req, res) => {
  try {
    const { userId, courseId, status } = req.body;

    // Validate input data
    if (!userId || !courseId || !status) {
      return res.status(400).json({ message: 'Missing required fields' });
    }

    // Check if the status is valid
    if (!['enrolled', 'completed', 'cancelled'].includes(status)) {
      return res.status(400).json({ message: 'Invalid status' });
    }

    // Find the user and course
    const user = await User.findById(userId);
    const course = await Course.findById(courseId);

    if (!user || !course) {
      return res.status(404).json({ message: 'User or course not found' });
    }

    // Find the enrollment object in user's enrollments array for the specified course
    const enrollmentIndex = user.enrollments.findIndex(enrollment => enrollment.courseId === courseId);

    if (enrollmentIndex === -1) {
      return res.status(404).json({ message: 'Enrollment not found for the specified course' });
    }

    // Update the enrollment status
    user.enrollments[enrollmentIndex].status = status;
    await user.save();

    return res.status(200).json({ message: 'Enrollment status updated successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};

//[End of Activity]