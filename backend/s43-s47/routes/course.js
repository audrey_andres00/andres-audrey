//[Activity - s45]

//Dependencies
const express = require("express");
const courseController = require("../controllers/course")
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing Component
const router = express.Router();

//Routes

//create a course
router.post("/", verify, verifyAdmin, courseController.addCourse);

//[End of Activity]

//route for retrieving all courses
router.get("/all", courseController.getAllCourses);

//[Mini Activity]
//create a route for getting all active courses
//use default endpoint
//getAllActiveCourses
router.get("/", courseController.getAllActiveCourses);

//get a specific course
router.get("/:courseId", courseController.getCourse);

//edit a specific course
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

//[Activity - S46]

//course archiving
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse)

//activating a course
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse)

//[End of Activity]

// Route to search for courses by course name
router.post('/search', courseController.searchCoursesByName);

// Route to  get all enrolled user to a certain course
router.get('/:courseId/enrolled-users', courseController.getEmailsOfEnrolledUsers);

//[Activity S48]

//Retrieve all the courses within the given price range
router.post('/search-courses', courseController.searchCoursesByPrice);

//[End of Activity]

//Export Route System
module.exports = router