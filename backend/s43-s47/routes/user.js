//Route
//Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user")
const auth = require("../auth");
const {verify, verifyAdmin} = auth;

//Routing Component
const router = express.Router();

//Routes - POST
//Check email
router.post("/checkEmail",(req,res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=>res.send(resultFromController))
})

//Register a user
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User Authentication
router.post("/login",userController.loginUser);

//[Activity] User Document/Data
router.post("/details", verify, userController.getProfile)
//[End of Activity]

//Route to enroll user to course
router.post("/enroll", verify, userController.enroll);

//[Activity - S47]

//retrieve all the courses in which the authenticated user is enrolled
router.get("/getEnrollments", verify, userController.getEnrollments);

//[End of Activity]

//Password Reset
router.post('/reset-password', verify, userController.resetPassword)

//Update Profile
router.put('/profile', verify, userController.updateProfile);

//[Activity S48]

//Update a regular user to an admin and send a message for successful update
// Route for updating a user as an admin
router.put('/update-to-admin', verify, verifyAdmin, userController.updateUserToAdmin);

//Update the enrollment status of a user for a specific course
// Route for updating enrollment status
router.put('/update-status', verify, verifyAdmin, userController.updateEnrollmentStatus);

//[End of Activity]

//Export Route System
module.exports = router