//Create a simple Express JS application

//Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
//Allows our backend application to be available to our frontend application

const userRoutes = require("./routes/user")

//Environment Setup
const port = 4000;

//Server Setup
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//Database Connection
	//Connection to our MongoDB Database
	mongoose.connect("mongodb+srv://yerdy777:55pm6blRpiMDrdzD@batch-297.vv7liy8.mongodb.net/S43-S47?retryWrites=true&w=majority",
		{
			useNewUrlParser:true,
			useUnifiedTopology:true
		}
	);

	//prompt
	mongoose.connection.once('open',()=>console.log('Now Connected to MongoDB Atlas'))

	//[Backend Routes]
	app.use("/users",userRoutes);
	app.use("/courses", require("./routes/course"))

//Server Gateway Response
if(require.main === module){
	app.listen(process.env.PORT || port, ()=>{
		console.log(`API is now online on port ${process.env.PORT || port}`)
	})
}

module.exports = app;