console.log("Hello")

//1. Write a simple JS function to join all elements of the following array into a string:


function colorString(){

	color = ["Blue","Red","Yellow","Orange","Green","Purple"];

	console.log("Colors in array: ");
	console.log(color);
	console.log("Colors in string:");
	console.log(color[0]+","+color[1]+","+color[2]+","+color[3]+","+color[4]+","+color[5]);
}

colorString();

//2. Create a function that will add a planet at the beginning of the array:

function firstPlanet(){

	planet = ["Venus","Mars","Earth"];

	console.log("Planets before: ");
	console.log(planet);

	planet.unshift("Mercury");

	console.log("Planets after: ");
	console.log(planet);
}

firstPlanet();

//3. Create an array of your favorite singers' names. Create a function that will remove the last name in the array. Then, replace the deleted name with other two names.


function replaceSinger(){

	let faveSingers = ["EXO","NIKI","Ariana Grande","Sabrina Carpenter","Taylor Swift"];
	faveSingers.pop()
	faveSingers.push("Troye Sivan","keshi");
	console.log(faveSingers);
}

replaceSinger();