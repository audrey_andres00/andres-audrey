//console.log("Test");

/*Create a student grading system using an arrow function that will receive the grade from a prompt. The grade categories are as follows:
- Novice (80-below)
- Apprentice (81-90)
- Master (91-100)*/

//let inputGrade = Number(prompt("Enter your grade:"));



const gradeCategory = () => {

	let inputGrade = Number(prompt("Enter your grade:"));

	if(inputGrade <= 80){
		return "Your grade is: " + inputGrade + " - NOVICE";
	}
	else if(inputGrade >= 81 && inputGrade <= 90){
		return "Your grade is: " + inputGrade + " - APPRENTICE";
	}
	else if(inputGrade >= 91 && inputGrade <= 100){
		return "Your grade is: " + inputGrade + " - MASTER";
	}
}

console.log(gradeCategory());