//console.log("Hello")

/*Create an odd-even checker function that will check which numbers from 1-50 are odd and which are even.

Sample output in the console: 
1 - The number 1 is odd 
2 - The number 2 is even 
3 - The number 3 is odd*/

function oddEvenChecker(){

	for(let num = 1; num <= 50; num++){

		if(num % 2 === 0){
			console.log(num + " - The number " + num + " is even");
		} else {
			console.log(num + " - The number " + num + " is odd");
		}
	}
}

oddEvenChecker();